<?php
    require('fpdf/fpdf.php');
    require('config.php'); 
    date_default_timezone_set("ASIA/JAKARTA");

function TanggalIndo($date){
	$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
 
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);
 
	$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
	return($result);
}

    $carikode = mysqli_query($koneksi, "SELECT id_surat from tb_surat") or die (mysqli_error());
    // menjadikannya array
    $datakode = mysqli_fetch_array($carikode);
    $jumlah_data = mysqli_num_rows($carikode);
    // jika $datakode
    if ($datakode) {
     // membuat variabel baru untuk mengambil kode barang mulai dari 1
     $nilaikode = substr($jumlah_data[0], 1);
     // menjadikan $nilaikode ( int )
     $kode = (int) $nilaikode;
     // setiap $kode di tambah 1
     $kode = $jumlah_data + 1;
     // hasil untuk menambahkan kode 
     // angka 3 untuk menambahkan tiga angka setelah B dan angka 0 angka yang berada di tengah
     // atau angka sebelum $kode
     $kode_otomatis =str_pad($kode, 4, "0", STR_PAD_LEFT);
    } else {
     $kode_otomatis = "0001";
    }


   

    $tgl_surat = date('Y-m-d');
    //kode berita yang akan dikonvert
    $sp_no = @$_GET['sp_no'];

    $sql = mysqli_query($koneksi,"SELECT tgl_berangkat,nomor_srtd, tgl_kembali,dasar_srt, 
    kota_asal, jml_hari, kota_tujuan, kendaraan, maksud_sp, tgl_surat,nama_dewan,jabatan_dewan,nama,jabatan
    
    FROM tb_sptd s
    INNER JOIN tb_dewan d ON s.id_dewan=d.id_dewan
    INNER JOIN tb_pegawai i ON s.id = i.id where sp_no='$sp_no'");

    $result = mysqli_fetch_array($sql);

    // pecah data array 


    $pdf = new FPDF('P','mm','A4');
    $pdf->SetMargins(20,20,10);
    $pdf->AliasNbPages();
    $pdf->AddPage();
    
    // Logo
    $pdf->Image('DPRD.png',35,19,18);
    // Arial bold 15
    $pdf->SetFont('Arial','B',12);
    // Move to the right
    $pdf->Cell(10);
    // Title
    $pdf->Cell(0,6,'PEMERINTAH  PROVINSI SULAWESI BARAT','0','20','C');
    $pdf->Cell(0,7,'SEKRETARIAT DEWAN PERWAKILAN RAKYAT DAERAH','0','20','C');
    $pdf->Cell(1);
    $pdf->SetFont('Arial','I',10);
    $pdf->Cell(0,4,'Alamat : Jl. Abd. Malik Patana Endeng, Rangas,  Mamuju  Kode Pos 91511','0','20','C');
    $pdf->Cell(160,0.8,'','0','20','C',true);
    $pdf->Ln(5);
    
    $pdf->SetFont('Times','U',16);
    $pdf->Cell(0,10,'S U R A T  T U G A S',0,1,'C');
    $pdf->SetFont('Times','',10);
    $pdf->Cell(0,3,'Nomor:'.$result['nomor_srtd'],0,1,'C');
    $pdf->Cell(15);
    
    $pdf->Cell(100 ,2,'',0,1);//end of line
    $pdf->SetFont('Times','',12);
    $pdf->Cell(10,9,'Dasar  :   ',0,0);
    $pdf->SetY(60);
    $pdf->SetX(35);
    $pdf->MultiCell(160,5,''.$result['dasar_srt'],0,'L');
    $pdf->Ln(5);
    
    // $pdf->Cell(0,8,'Dalam rangka melaksanakan tugas sekretariat DPRD sebagai unsur pendukung pelaksanaan tugas',0,1,'A');
    // $pdf->Cell(0,5,'Dan fungsi DPRD Provinsi Sulawesi Barat, maka dengan  ini:',0,1,'A');
    // $pdf->Cell(0,8,'Sekretaris DPRD Provinsi Sulawesi Barat:',0,1,'A');
   
    $pdf->SetFont('Times','B',12);
    $pdf->Cell(0,10,'M E N U G A S K A N',0,1,'C');
    
    
    $pdf->Cell(100 ,2,'',0,1);//end of line
    $pdf->SetFont('Times','',12);
    $pdf->Cell(35 ,5,'Nama',0,0);
    $pdf->Cell(2,5,':',0,0);
    $pdf->Cell(90 ,5,''.$result['nama'],0,1);
    $pdf->Cell(100 ,2,'',0,1);//end of line
    $pdf->Cell(35 ,5,'Jabatan',0,0);
    $pdf->Cell(2,5,':',0,0);
    $pdf->Cell(90 ,5,''.$result['jabatan'],0,1);
    $pdf->Cell(100 ,2,'',0,1);//end of line
    $pdf->Cell(35 ,5,'Kota Asal',0,0);
    $pdf->Cell(2,5,':',0,0);
    $pdf->Cell(90 ,5,''.$result['kota_asal'],0,1);
    $pdf->Cell(100 ,2,'',0,1);//end of line
    $pdf->Cell(35 ,5,'Kota Tujuan',0,0);
    $pdf->Cell(2,5,':',0,0);
    $pdf->Cell(90 ,5,''.$result['kota_tujuan'],0,1);
    $pdf->Cell(100 ,2,'',0,1);//end of line

    $pdf->Cell(35 ,5,'Maksud:',0,0);
    $pdf->Cell(2,5,':',0,0);
    $pdf->MultiCell(135,5,''.$result['maksud_sp'],0,1);
    $pdf->Cell(100 ,2,'',0,1);//end of line

    $pdf->Cell(35 ,5,'Lamanya ',0,0);
    $pdf->Cell(2,5,':',0,0);
    $pdf->Cell(90 ,5,''.$result['jml_hari'] .' (Hari)',0,1);
    $pdf->Cell(100 ,2,'',0,1);//end of line
    
    $pdf->Cell(35 ,5,'Berangkat Tanggal',0,0);
    $pdf->Cell(2,5,':',0,0);
    $pdf->Cell(90 ,5,''.TanggalIndo($result['tgl_berangkat']),0,1);
    $pdf->Cell(100 ,2,'',0,1);//end of line
    $pdf->Cell(35 ,5,'Kembali Tanggal',0,0);
    $pdf->Cell(2,5,':',0,0);
    $pdf->Cell(90 ,5,''.TanggalIndo($result['tgl_kembali']),0,1);
    $pdf->Cell(100 ,2,'',0,1);//end of line

    $pdf->Cell(0,8,'Nama Pengikut              :',0,1,'A');
    $pdf->Cell(0,10,'Anggota                   :',0,1,'A');
    $no=1;
    $sql = mysqli_query($koneksi," SELECT *
    FROM tb_sptd s
    INNER JOIN tb_pengikut p ON s.sp_no = p.sp_no
    WHERE nomor_srtd ");
    while($k=mysqli_fetch_array($sql)){
    $pdf->Cell(0,8,'                                         '.$no++.'.   '.$k['nama_pengikut'],0,1,'A');
       
    } 
    $pdf->Cell(0,10,'Demikian Surat Tugas ini dibuat untuk dilaksanakan dan dipergunakan seperlunya.',0,1,'A');
    $pdf->Ln(5);

    $pdf->SetY(190);
    $pdf->SetX(140);
    $pdf->Cell(0,4,'Dikeluarkan  :  Di  '.$result['kota_asal'],0,1);

    $pdf->SetY(193);
    $pdf->SetX(140);
    $pdf->SetFont('Times','U');
    $pdf->Cell(0,8,'Pada tanggal,  '.TanggalIndo($result['tgl_surat']),0,1);
    $pdf->SetFont('Times','B',12);

    $pdf->SetY(200);
    $pdf->SetX(110);
    $pdf->Cell(0,6.5,'DEWAN PERWAKILAN RAKYAT DAERAH',0,1,'C');
    
    $pdf->SetY(205);
    $pdf->SetX(110);
    $pdf->Cell(0,6.5,'PROVINSI SULAWESI BARAT',0,1,'C');
   
    $pdf->SetY(210);
    $pdf->SetX(110);
    $pdf->Cell(0,6.5,''.$result['jabatan'],0,1,'C');


    $pdf->SetY(220);
    $pdf->SetX(110);
    $pdf->SetFont('Times','U',14);
    $pdf->Cell(0,40,''.$result['nama_dewan'],0,1,'C');

    // $pdf->SetY(235);
    // $pdf->SetX(10);
    // $pdf->SetFont('Times','',7);
    // $pdf->Cell(0,40,''.$kode_otomatis,0,1);

$pdf->Output();   
?>