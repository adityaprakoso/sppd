<?php
    require('fpdf/fpdf.php');
    require('config.php'); 
    $pdf = new FPDF('L','mm','Legal');
    $pdf->SetMargins(10,10,20);
    $pdf->AliasNbPages();
    $pdf->AddPage();

    $id_surat   = @$_GET['id_surat'];

    $sql = mysqli_query($koneksi,"SELECT tgl_berangkat, nomor_srt, tgl_kembali,dasar_srt,kota_asal, jml_hari, 
    kota_tujuan, kendaraan, maksud_sp, tgl_surat, nama_staf,pangkat_gol_staf,nip_staf,tingkat,
    jabatan_staf, nama_spt, nip_spt

    FROM tb_surat s
    INNER JOIN tb_staf d ON s.id_staf = d.id_staf
    INNER JOIN tb_spt i ON s.id_spt = i.id_spt
    WHERE id_surat='$id_surat'");

    $result = mysqli_fetch_array($sql);
    
    // Logo
    $pdf->Image('logo.jpg',14,9,16);
    // Arial bold 15
    $pdf->SetFont('Arial','B',11);
    // Move to the right
    $pdf->Cell(10);
    // Title
    $pdf->SetFont('Arial','',11);
    $pdf->Cell(140,4,'PEMERINTAH  PROVINSI SULAWESI BARAT','0','20','C');
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(140,4,'SEKRETARIAT DEWAN PERWAKILAN RAKYAT DAERAH','0','20','C');
    $pdf->Cell(1);
    $pdf->SetFont('Arial','I',10);
    $pdf->Cell(140,4,'Alamat : Jl. Abd. Malik Patana Endeng NO.02 Rangas Mamuju','0','20','C');
    $pdf->Cell(140,4,'Telp/Fax : 0426-2325292 kode Pos:911511','0','20','C');
    $pdf->Ln(5);

    $pdf->SetLineWidth(1);
    $pdf->Line(9,27,165,27);
    $pdf->SetLineWidth(0);
    $pdf->Line(9,28,165,28);

    $pdf->SetFont('Arial','',10);
    $pdf->Cell(130,2,'Nomor: '.$result['nomor_srt'],0,1,'C');
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(150,10,'SURAT PERINTAH PERJALANAN DINAS (SPPD)',0,1,'C');

    $pdf->SetFont('Arial','',8);
    $pdf->Cell(10,6,'1',1,0,'C');
    $pdf->Cell(70,6,'Pengguna Anggaran',1,0,'L');
    $pdf->Cell(75,6,'Sekertaris DPRD Provinsi Sul-Bar',1,1,'L');

    $pdf->SetFont('Arial','',8);
    $pdf->Cell(10,4,'2',1,0,'C');
    $pdf->Cell(70,4,'Nama Anggota DPRD',1,0,'L');
    $pdf->Cell(75,4,''.$result['nama_staf'],1,1,'L');

    $pdf->Cell(10,15,'3',1,0,'C');
    $pdf->SetX(20);
    $pdf->MultiCell(70,5,'a. Pangkat dan Golongan
b. Jabatan/Instansi 
c. Tingkat Biaya Perjalanan Dinas ',1);

    $pdf->SetY(53);
    $pdf->SetX(90);
$pdf->MultiCell(75,5,'a.  '.$result['pangkat_gol_staf'].'
'.'b.  '.$result['jabatan_staf'].'
'.'c.  '.$result['tingkat'],1,'L');

$cellWidth=75; //lebar sel
$cellHeight=4; //tinggi sel satu baris normal

//periksa apakah teksnya melibihi kolom?
if($pdf->GetStringWidth($result['maksud_sp']) < $cellWidth){
    //jika tidak, maka tidak melakukan apa-apa
    $line=1;
}else{
    //jika ya, maka hitung ketinggian yang dibutuhkan untuk sel akan dirapikan
    //dengan memisahkan teks agar sesuai dengan lebar sel
    //lalu hitung berapa banyak baris yang dibutuhkan agar teks pas dengan sel
    
    $textLength=strlen($result['maksud_sp']);	//total panjang teks
    $errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
    $startChar=0;		//posisi awal karakter untuk setiap baris
    $maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
    $textArray=array();	//untuk menampung data untuk setiap baris
    $tmpString="";		//untuk menampung teks untuk setiap baris (sementara)
    
    while($startChar < $textLength){ //perulangan sampai akhir teks
        //perulangan sampai karakter maksimum tercapai
        while( 
        $pdf->GetStringWidth( $tmpString ) < ($cellWidth-$errMargin) &&
        ($startChar+$maxChar) < $textLength ) {
            $maxChar++;
            $tmpString=substr($result['maksud_sp'],$startChar,$maxChar);
        }
        //pindahkan ke baris berikutnya
        $startChar=$startChar+$maxChar;
        //kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
        array_push($textArray,$tmpString);
        //reset variabel penampung
        $maxChar=0;
        $tmpString='';
        
    }
    //dapatkan jumlah baris
    $line=count($textArray);
}
//tulis selnya
$pdf->SetFillColor(255,255,255);
$pdf->Cell(10,($line * $cellHeight),'4',1,0,'C',true); //sesuaikan ketinggian dengan jumlah garis
$pdf->Cell(70,($line * $cellHeight),'Maksud Perjalanan Dinas',1,0); //sesuaikan ketinggian dengan jumlah garis

$xPos=$pdf->GetX();
$yPos=$pdf->GetY();
$pdf->MultiCell($cellWidth,$cellHeight,$result['maksud_sp'],1,1);


$pdf->SetFont('Arial','',8);
$pdf->Cell(10,5,'5',1,0,'C');
$pdf->Cell(70,5,'Jenis angkutan yang digunakan',1,0,'L');
$pdf->Cell(75,5,''.$result['kendaraan'],1,1,'L');

$xPos=$pdf->GetX();
$yPos=$pdf->GetY();

$pdf->Cell(10,10,'6',1,0,'C');
$pdf->MultiCell(70,5,'a. Tempat berangkat
b.Tempat tujuan',1,'L');

$pdf->SetXY($xPos +80, $yPos);
$pdf->MultiCell(75,5,'a.  '.$result['kota_asal'].'
'.'b.  '.$result['kota_tujuan'],1,'L');

$xPos=$pdf->GetX();
$yPos=$pdf->GetY();
$pdf->Cell(10,15,'7',1,0,'C');
$pdf->MultiCell(70,5,'a. lamanya Perjalanan Dinas 
b. Tgl.Berangkat 
c. Tgl.Kembali',1,'L');

$pdf->SetXY($xPos + 80 , $yPos);
$pdf->MultiCell(75,5,'a.  '.$result['jml_hari']. ' (Hari)
'.'b.  '.date('d F Y', strtotime($result['tgl_berangkat'])).'
'.'c.  '.date('d F Y', strtotime($result['tgl_kembali'])),1,'L');


$pdf->SetFont('Arial','',8);
$pdf->Cell(10,5,'8',1,0,'C');
$pdf->Cell(70,5,'Pengikut:Nama ',1,0,'L');
$pdf->Cell(37.5,5,'Tanggal lahir',1,0,'C');
$pdf->Cell(37.5,5,'Keterangan',1,1,'C');

$xPos=$pdf->GetX();
$yPos=$pdf->GetY();
$pdf->SetFont('Arial','',8);
$pdf->Cell(10,10,'',1,0,'C');
$pdf->MultiCell(70,5,'1. 
2.',1,'L');

$pdf->SetXY($xPos + 80 , $yPos);
$pdf->Cell(37.5,10,'',1,0,'C');
$pdf->Cell(37.5,10,'',1,1,'C');

$xPos=$pdf->GetX();
$yPos=$pdf->GetY();
$pdf->SetFont('Arial','',8);
$pdf->Cell(10,15,'9',1,0,'C');
$pdf->MultiCell(70,5,'Pembebanan Anggarana. 
a.SKPD 
b.Akun',1,'L');

$pdf->SetXY($xPos + 80 , $yPos);
$pdf->Cell(75,15,'',1,1,'L');

$pdf->SetFont('Arial','',8);
$pdf->Cell(10,5,'10',1,0,'C');
$pdf->Cell(70,5,'Keterangan Lain-lain',1,0,'L');
$pdf->Cell(75,5,'Lihat Sebelah',1,1,'L');

$pdf->SetFont('Arial','',8);
$pdf->Cell(40,5,'Coret yang tidak perlu*)',0,1,'L');
$pdf->SetFont('Times','',8);
$pdf->SetY(155);
$pdf->SetX(21);
$pdf->Cell(120,2,'Di keluarkan : Di  '.$result['kota_asal'],0,1,'R');
$pdf->SetY(157);
$pdf->SetX(27);
$pdf->SetFont('Times','U',8);
$pdf->Cell(116.3,5,'Pada tanggal :  '.date('d F Y', strtotime($result['tgl_surat'])),0,1,'R');
$pdf->ln(-20);
$pdf->SetFont('Times','',8);
$pdf->Cell(139.3,45,'Sekretaris DPRD Provinsi Sul-Bar',0,1,'R');
$pdf->SetFont('Times','U',8);
$pdf->Cell(140,0,'Drs. H ABDUL WAHAB HS,M.Si',0,1,'R');
$pdf->SetFont('Times','',8);
$pdf->Cell(134,6,'Pangkat : Pembina Tk1 / IV.b',0,1,'R');
$pdf->SetFont('Times','',8);
$pdf->Cell(138.6,1,'NIP       : 19711018 199101 1 001',0,1,'R');
$pdf->ln(-20);

    // untuk halaman kedua
    $pdf->SetY(20);
    $pdf->SetX(180);
    $pdf->SetFont('Arial','',8);
    $pdf->Cell(80,30,'',1,0);
    $pdf->MultiCell(80,3,'I. Berangkat Dari               : Mamuju Sulawesi Barat         (Tempat Kedudukan)                                                                   Ke                                    :                                                 Pada Tangggal                 :                                                Sekretaris DPRD Provinsi Sulawesi Barat                                                             
                                                                                                                                                                                   Drs.H.ABDUL.WAHAB HS. M.Si                                          NIP : 19711018 199101 1 001',1,1);
                                                                                                                                                                                   
    $pdf->SetY(50);
    $pdf->SetX(180);
    $pdf->SetFont('Arial','',8);
    $pdf->MultiCell(80,3,'II   Berangkat Dari               : Mamuju Sulawesi Barat         
    Pada Tangggal                :                                                
    Kepala                             :                                                             
    
    
    
    
    (..............................................)
    NIP : ',1,1);
    $pdf->SetY(50);
    $pdf->SetX(260);
    $pdf->MultiCell(80,3,'    Berangkat Dari                   : Mamuju Sulawesi Barat
    Ke                                        :                                                
    Pada Tangggal                    :                                                
    Kepala                                                             
      
    
    
    
    (..............................................)
    NIP : ',1,1);
    $pdf->SetY(80);
    $pdf->SetX(180);
    $pdf->SetFont('Arial','',8);
    $pdf->MultiCell(80,3,'III  Berangkat Dari                   : Mamuju Sulawesi Barat
    Pada Tangggal                    :                                                
    Kepala                                                             
      
    
    
    
    
    (..............................................)
    NIP : ',1,1);
    $pdf->SetY(80);
    $pdf->SetX(260);
    $pdf->MultiCell(80,3,'Berangkat Dari                    : Mamuju Sulawesi Barat         (Tempat Kedudukan)                                                                  Ke                                        :                                                Pada Tangggal                    :                                                Sekretaris DPRD Provinsi Sulawesi Barat                                                             
                                                                                                                                                                                        (..............................................)
    NIP : ',1,1);
    $pdf->SetY(110);
    $pdf->SetX(180);
    $pdf->SetFont('Arial','',8);
    $pdf->MultiCell(80,3,'IV   Tiba di 
    (Tempat Kedudukan : Mamuju (Sul-Bar) 
    Pada Tanggal           :
    Penguna Anggaran    
    
    
    
    
    Drs.H.ABD. WAHAB, HS,M,SI 
    Pangkat : Pembina TK I/IV b 
    NIP : 197110181991011001',1,1);
    
    $pdf->SetY(110);
    $pdf->SetX(260);
    $pdf->MultiCell(80,3,'Telah Diperiksa dengan keterangan bahwa perjalanan tersebut atas perintah pejabat yang berwenang dan semata-mata untuk kepentingan jabatab dalam waktu yang sesingkat-singkatnya.                                 
    
    Pengguna Anggaran,                                                             
    
    
    (Drs.H.ABDUL.WAHAB HS. M.Si)
    Pangkat : Pembina TK I/IV b
    NIP : 19711018 199101 1 001',1,1);

    $pdf->SetY(143);
    $pdf->SetX(180);
    $pdf->Cell(160,5,'Catatan Lain Lain',1,1);
    $pdf->SetFont('Arial','',9);
    $pdf->SetY(148);
    $pdf->SetX(180);
    $pdf->MultiCell(160,4,'VII. PERHATIAN
    Pengguna Anggaran/Kuasa penguuna anggaran yang menerbitkan SPPD, Kepala Daerah/Wakil Kepala Daerah Pimpinan dan Anggota DPRD, PNS dan PTT yang melakukan perjalanan dinas, para pejabat yang mengesahkan tanggal berangkat/tiba, serta bendahara pengeluaran bertanggungjawab berdasarkan keungan Daerah apabila daerah menderita rugi akibat, kelalaian, dan Kealpaannya.',1,1);

$pdf->Output();
?>