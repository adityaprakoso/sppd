<?php
     require('config.php');
     $filename = "Surat_dewan_exel-(".date('d-m-Y').").xls";
     header("content-disposition: attachment; filename='$filename'");
     header("content-type: application/vdn.ms-exel");
?>
<h2>Laporan Surat Dewan</h2>
<table border="1">
<thead>
    <tr>
        <th>No</th>
        <th>Nomor Surat</th>
        <th>Nama Pelaksana</th>
        <th>Maksud</th>
        <th>Tanggal Berangkat</th>
        <th>Tanggal Kembali</th>
    </tr>
    </thead>
    <tbody>
    <?php
            $no=1;
            $sql=$koneksi->query("SELECT * FROM tb_sptd s 
            INNER JOIN tb_dewan d ON s.id_dewan = d.id_dewan
            where sp_no");
            while ($data= $sql->fetch_assoc()) {
        ?>
        <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $data['nomor_srtd']; ?></td>
            <td><?php echo $data['nama_dewan']; ?></td>
            <td><?php echo $data['maksud_sp']; ?></td>
            <td><?php echo date('d F Y', strtotime($data['tgl_berangkat'])); ?></td>
            <td><?php echo date('d F Y', strtotime($data['tgl_kembali'])); ?></td>
        </tr>
        <?php }
        ?>
</tbody>
</table>