<?php
require('fpdf/fpdf.php');
require('config.php'); 
$pdf = new FPDF('L','mm','Legal');
$pdf->SetMargins(10,10,-10);
$pdf->AliasNbPages();
$pdf->AddPage();
date_default_timezone_set("ASIA/JAKARTA");
function TanggalIndo($date){
	$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
 
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);
 
	$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
	return($result);
}


$sp_no   = @$_GET['sp_no'];

    $sql = mysqli_query($koneksi,"SELECT tgl_berangkat, nomor_srtd, tgl_kembali,dasar_srt,tingkat,dasar_srt,tingkat, 
    kota_asal, jml_hari, kota_tujuan, kendaraan, maksud_sp, tgl_surat,nama_dewan,jabatan_dewan,nama,jabatan
    
    FROM tb_sptd s
    -- INNER JOIN tb_pengikut p ON s.sp_no = p.sp_no
    INNER JOIN tb_dewan d ON s.id_dewan=d.id_dewan
    INNER JOIN tb_pegawai i ON s.id = i.id where sp_no='$sp_no'");

    $result = mysqli_fetch_array($sql);
       // Logo
   $pdf->Image('DPRD.png',14,9,16);
   // Arial bold 15
   $pdf->SetFont('Arial','B',11);
   // Move to the right
   $pdf->Cell(10);
   // Title
   $pdf->SetFont('Arial','',11);
   $pdf->Cell(140,4,'PEMERINTAH  PROVINSI SULAWESI BARAT','0','20','C');
   $pdf->SetFont('Arial','B',11);
   $pdf->Cell(140,4,'DEWAN PERWAKILAN RAKYAT DAERAH','0','20','C');
   $pdf->Cell(1);
   $pdf->SetFont('Arial','I',10);
   $pdf->Cell(140,4,'Alamat : Jl. Abd. Malik Patana Endeng NO.02 Rangas Mamuju','0','20','C');
   $pdf->Cell(140,4,'Telp/Fax : 0426-2325292 kode Pos:911511','0','20','C');
   $pdf->Ln(5);

   $pdf->SetLineWidth(1);
   $pdf->Line(9,27,165,27);
   $pdf->SetLineWidth(0);
   $pdf->Line(9,28,165,28);

   $pdf->SetFont('Arial','',10);
   $pdf->Cell(130,2,'Nomor: '.$result['nomor_srtd'],0,1,'C');
   $pdf->SetFont('Arial','B',10);
   $pdf->Cell(150,10,'SURAT PERINTAH PERJALANAN DINAS (SPPD)',0,1,'C');

   
   $pdf->SetFont('Arial','',8);
   $pdf->Cell(10,5,'1',1,0,'C');
   $pdf->Cell(50,5,'Pengguna Anggaran',1,0,'L');
   $pdf->Cell(95,5,'Sekertaris DPRD Provinsi Sul-Bar',1,1,'L');
   $pdf->SetFont('Arial','',8);
   $pdf->Cell(10,5,'2',1,0,'C');
   $pdf->Cell(50,5,'Nama Anggota DPRD',1,0,'L');
   $pdf->Cell(95,5,''.$result['nama'],1,1,'L');

   $pdf->Cell(10,15,'3',1,0,'C');
   $pdf->SetX(20);
   $pdf->MultiCell(50,5,'a. Pangkat dan Golongan
b. Jabatan/Instansi 
c. Tingkat Biaya Perjalanan Dinas ',1);

   $pdf->SetY(53);
   $pdf->SetX(70);
$pdf->MultiCell(95,5,'a. Pangkat Dan golongan
'.'b.  '.$result['jabatan'].'
'.'c.  '.$result['tingkat'],1,'L');

   $cellWidth=95; //lebar sel
	$cellHeight=4; //tinggi sel satu baris normal
	//periksa apakah teksnya melibihi kolom?
	if($pdf->GetStringWidth($result['maksud_sp']) < $cellWidth){
		//jika tidak, maka tidak melakukan apa-apa
		$line=1;
	}else{
		//jika ya, maka hitung ketinggian yang dibutuhkan untuk sel akan dirapikan
		//dengan memisahkan teks agar sesuai dengan lebar sel
		//lalu hitung berapa banyak baris yang dibutuhkan agar teks pas dengan sel
		
		$textLength=strlen($result['maksud_sp']);	//total panjang teks
		$errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
		$startChar=0;		//posisi awal karakter untuk setiap baris
		$maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
		$textArray=array();	//untuk menampung data untuk setiap baris
		$tmpString="";		//untuk menampung teks untuk setiap baris (sementara)
		
		while($startChar < $textLength){ //perulangan sampai akhir teks
			//perulangan sampai karakter maksimum tercapai
			while( 
			$pdf->GetStringWidth( $tmpString ) < ($cellWidth-$errMargin) &&
			($startChar+$maxChar) < $textLength ) {
				$maxChar++;
				$tmpString=substr($result['maksud_sp'],$startChar,$maxChar);
			}
			//pindahkan ke baris berikutnya
			$startChar=$startChar+$maxChar;
			//kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
			array_push($textArray,$tmpString);
			//reset variabel penampung
			$maxChar=0;
			$tmpString='';
			
		}
		//dapatkan jumlah baris
		$line=count($textArray);
	}
    //tulis selnya
$pdf->SetFillColor(255,255,255);
$pdf->Cell(10,($line * $cellHeight),'4',1,0,'C',true); //sesuaikan ketinggian dengan jumlah garis
$pdf->Cell(50,($line * $cellHeight),'Maksud Perjalanan Dinas',1,0); //sesuaikan ketinggian dengan jumlah garis
   
$xPos=$pdf->GetX();
$yPos=$pdf->GetY();
$pdf->MultiCell($cellWidth,$cellHeight,$result['maksud_sp'],1,1);


    $pdf->SetFont('Arial','',8);
    $pdf->Cell(10,5,'5',1,0,'C');
    $pdf->Cell(50,5,'Jenis angkutan yang digunakan',1,0,'L');
    $pdf->Cell(95,5,''.$result['kendaraan'],1,1,'L');
 
    $xPos=$pdf->GetX();
    $yPos=$pdf->GetY();
    
    $pdf->Cell(10,10,'6',1,0,'C');
    $pdf->MultiCell(50,5,'a. Tempat berangkat
b.Tempat tujuan',1,'L');
   
   $pdf->SetXY($xPos + 60, $yPos);
   $pdf->MultiCell(95,5,'a.  '.$result['kota_asal'].'
'.'b.  '.$result['kota_tujuan'],1,'L');

$xPos=$pdf->GetX();
$yPos=$pdf->GetY();
$pdf->Cell(10,15,'7',1,0,'C');
$pdf->MultiCell(50,5,'a. lamanya Perjalanan Dinas 
b. Tgl.Berangkat 
c. Tgl.Kembali',1,'L');

$pdf->SetXY($xPos + 60 , $yPos);
$pdf->MultiCell(95,5,'a.  '.$result['jml_hari']. ' (Hari)
'.'b.  '.TanggalIndo($result['tgl_berangkat']).'
'.'c.  '.TanggalIndo($result['tgl_kembali']),1,'L');


$pdf->SetFont('Arial','',8);
$pdf->Cell(10,5,'8',1,0,'C');
$pdf->Cell(50,5,'Pengikut:Nama ',1,0,'L');
$pdf->Cell(47.5,5,'Tanggal lahir',1,0,'C');
$pdf->Cell(47.5,5,'Keterangan',1,1,'C');

$xPos=$pdf->GetX();
$yPos=$pdf->GetY();
$pdf->SetFont('Arial','',8);
$pdf->Cell(10,10,'',1,0,'C');
$pdf->MultiCell(50,5,'1. 
2.',1,'L');

$pdf->SetXY($xPos + 60 , $yPos);
$pdf->Cell(47.5,10,'',1,0,'C');
$pdf->Cell(47.5,10,'',1,1,'C');

$xPos=$pdf->GetX();
$yPos=$pdf->GetY();
$pdf->SetFont('Arial','',8);
$pdf->Cell(10,15,'9',1,0,'C');
$pdf->MultiCell(50,5,'Pembebanan Anggarana. 
a.SKPD 
b.Akun',1,'L');

$pdf->SetXY($xPos + 60 , $yPos);
$pdf->Cell(95,15,'',1,1,'L');

$pdf->SetFont('Arial','',8);
   $pdf->Cell(10,5,'10',1,0,'C');
   $pdf->Cell(50,5,'Keterangan Lain-lain',1,0,'L');
   $pdf->Cell(95,5,'Lihat Sebelah',1,1,'L');
   
   $pdf->SetFont('Arial','',8);
   $pdf->Cell(40,5,'Coret yang tidak perlu*)',0,1,'L');
   
   $pdf->SetFont('Times','',8);
   $pdf->SetY(155);
   $pdf->SetX(108);
   $pdf->Cell(50,2,'Di keluarkan : Di  '.$result['kota_asal'],0,1,'L');
   $pdf->SetFont('Times','U',8);
   $pdf->SetY(158);
   $pdf->SetX(108);
   $pdf->SetFont('Times','U',8);
   $pdf->Cell(50.3,5,'Pada tanggal :  '.TanggalIndo($result['tgl_surat']),0,1,'L');
   
   $pdf->ln(-20);
   $pdf->SetFont('Times','',8);
   $pdf->Cell(139.3,45,'Sekretaris DPRD Provinsi Sul-Bar',0,1,'R');
   $pdf->SetFont('Times','U',8);
   $pdf->Cell(140,0,'Drs. H ABDUL WAHAB HS,M.Si',0,1,'R');
   $pdf->SetFont('Times','',8);
   $pdf->Cell(134,6,'Pangkat : Pembina Tk1 / IV.b',0,1,'R');
   $pdf->SetFont('Times','',8);
   $pdf->Cell(138.6,1,'NIP       : 19711018 199101 1 001',0,1,'R');
   $pdf->ln(-20);

   
   // untuk halaman kedua
   $pdf->SetY(10);
   $pdf->SetX(180);
   $pdf->SetFont('Arial','',7);
   $pdf->Cell(80,30,'',1,0);
   $pdf->MultiCell(80,3,'I. Tiba Di               :          
   (Tempat Kedudukan)
   Ke                                   :                                                 
   Pada Tangggal               :                                                
   Kepala
   
   
   
   Drs.H.ABDUL.WAHAB HS. M.Si                                          
   NIP : 19711018 199101 1 001',1,1);
                                                                                                                                                                                  
   $pdf->SetY(40);
   $pdf->SetX(180);
   $pdf->SetFont('Arial','',7);
   $pdf->MultiCell(80,3,'II   Tiba Di                             :          
   Pada Tangggal                :                                                
   Kepala                             :                                                             
   
   

   (..............................................)
   NIP : ',1,1);
   
   $pdf->SetY(40);
   $pdf->SetX(260);
   $pdf->MultiCell(80,3,'    Berangkat Dari                   : 
   Ke                                        :                                                
   Pada Tangggal                    :                                                
   Kepala                                                             
     
   
   (..............................................)
   NIP : ',1,1);

   $pdf->SetY(64);
   $pdf->SetX(180);
   $pdf->SetFont('Arial','',7);
   $pdf->MultiCell(80,3,'III   Tiba Di                            :          
   Pada Tangggal                :                                                
   Kepala                             :                                                             
   
   

   (..............................................)
   NIP : ',1,1);
   
   $pdf->SetY(64);
   $pdf->SetX(260);
   $pdf->MultiCell(80,3,'    Berangkat Dari                   : 
   Ke                                        :                                                
   Pada Tangggal                    :                                                
   Kepala                                                             
     
   
   (..............................................)
   NIP : ',1,1);

   $pdf->SetY(88);
   $pdf->SetX(180);
   $pdf->SetFont('Arial','',7);
   $pdf->MultiCell(80,3,'IV  Tiba Di                              : 
   Pada Tangggal                    :                                                
   Kepala                                                             
     
   


   (..............................................)
   NIP : ',1,1);

   $pdf->SetY(88);
   $pdf->SetX(260);
   $pdf->MultiCell(80,3,'Berangkat Dari                           : 
   (Tempat Kedudukan)
   Ke                                        :
   Pada Tangggal                    :
   Kepala
   
   
   (..............................................)
   NIP : ',1,1);

   $pdf->SetY(115);
   $pdf->SetX(180);
   $pdf->SetFont('Arial','',7);
   $pdf->MultiCell(80,3,'V  Tiba Di                                : 
   Pada Tangggal                    :                                                
   Kepala                                                             
     
   


   (..............................................)
   NIP : ',1,1);

   $pdf->SetY(115);
   $pdf->SetX(260);
   $pdf->MultiCell(80,3,'Berangkat Dari                    :          
   (Tempat Kedudukan)
   Ke                                        :
   Pada Tangggal                    :
   Kepala
   
   
   (..............................................)
   NIP : ',1,1);

   $pdf->SetY(142);
   $pdf->SetX(180);
   $pdf->SetFont('Arial','',7);
   $pdf->MultiCell(80,3,'VI  Tiba Di
   (Tempat Kedudukan)  :
   Pada Tanggal        :
   Pengguna Anggaran
   
   
   
   (Drs.H.ABDUL.WAHAB HS. M.Si)
   Pangkat : Pembina TK I/IV b
   NIP : 19711018 199101 1 001',1,1);
   
   $pdf->SetY(142);
   $pdf->SetX(260);
   $pdf->MultiCell(80,3,'Telah Diperiksa dengan keterangan bahwa perjalanan tersebut atas perintah pejabat yang berwenang dan semata-mata untuk kepentingan jabatab dalam waktu yang sesingkat-singkatnya.                                 
   Pengguna Anggaran,                                                             
   
   

   (Drs.H.ABDUL.WAHAB HS. M.Si)
   Pangkat : Pembina TK I/IV b
   NIP : 19711018 199101 1 001',1,1);

   $pdf->SetY(172);
   $pdf->SetX(180);
   $pdf->Cell(160,5,'Catatan Lain Lain',1,1);
   $pdf->SetFont('Arial','',7);
   $pdf->SetY(177);
   $pdf->SetX(180);
   $pdf->MultiCell(160,4,'VII. PERHATIAN
        Pengguna Anggaran/Kuasa penguuna anggaran yang menerbitkan SPPD, Kepala Daerah/Wakil Kepala Daerah Pimpinan dan Anggota DPRD, PNS dan PTT yang melakukan perjalanan dinas, para pejabat yang mengesahkan tanggal berangkat/tiba, serta bendahara pengeluaran bertanggungjawab berdasarkan keungan Daerah apabila daerah menderita rugi akibat, kelalaian, dan Kealpaannya.',1,1);

$pdf->Output();
?>