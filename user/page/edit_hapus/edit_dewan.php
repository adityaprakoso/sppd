<script>
  $( function() {
    $( "#date" ).datepicker({
      dateFormat: "yy-mm-dd"
    });
  } );
  </script>

<?php

    $id = @$_GET['id'];
    $sql  = $koneksi->query("select * from tb_pegawai where id='$id'");
    $data = $sql->fetch_assoc(); 

?>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Daftar Dewan</h4>
                <p class="card-category">*Input Data Harus Benar</p>
            </div>
            <div class="card-body">
            <form  method="post">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="">Nama Dewan</label>
                            <input type="text" class="form-control border-input"  name="nama"  value="<?php echo $data['nama'];?>" >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="">Jabatan</label>
                            <input  type="text" class="form-control border-input" name="jabatan" value="<?php echo $data['jabatan'];?>">
                        </div>
                    </div>
                </div>
                <div>
                <input type="reset" name="reset" class="btn btn-secondary btn-fill btn-wd">
                <input type="submit" name="kirim" value="Simpan" class="btn btn-info btn-fill btn-wd">   
            </div>
            </form>
        </div>
        </div>
    </div>
</div>

<?php
if(isset($_POST['kirim'])){
$nama           = @$_POST['nama'];
$jabatan       = @$_POST['jabatan'];
    $sql = $koneksi->query("update tb_pegawai set nama='$nama', jabatan='$jabatan' where id='$id'");
    echo "<script>window.alert('Data Berhasil Dirubah')
    window.location='?halaman=master'</script>";

}
?>