<script>
  $( function() {
    $( "#date" ).datepicker({
      dateFormat: "yy-mm-dd"
    });
  } );
  </script>

<?php

    $id_dewan = @$_GET['id_dewan'];
    $sql  = $koneksi->query("select * from tb_dewan where id_dewan='$id_dewan'");
    $data = $sql->fetch_assoc(); 

?>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Daftar Data Dewan</h4>
            </div>
            <div class="card-body">
            <form  method="post">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="">Nama Dewan</label>
                            <input type="text" class="form-control border-input" name="nama_dewan" value="<?php echo $data['nama_dewan'];?>" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="">jabatan Dewan</label>
                            <input  type="text" id="date" class="form-control border-input" name="jabatan_dewan"  value="<?php echo $data['jabatan_dewan'];?>" required>
                        </div>
                    </div>
                </div>
                <div>
                <input type="reset" name="reset" class="btn btn-secondary btn-fill btn-wd">
                <input type="submit" name="kirim" value="Simpan" class="btn btn-info btn-fill btn-wd">   
            </div>
            </form>
        </div>
        </div>
    </div>
</div>

<?php
if(isset($_POST['kirim'])){
$nama_dewan           = @$_POST['nama_dewan'];
$jabatan_dewan       = @$_POST['jabatan_dewan'];

$sql = $koneksi->query("update tb_dewan set nama_dewan='$nama_dewan', jabatan_dewan='$jabatan_dewan' where id_dewan='$id_dewan'");
echo "<script>window.alert('Data Berhasil Dirubah')
window.location='?halaman=dewan'</script>";

}
?>