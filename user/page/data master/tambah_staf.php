<script>
  $( function() {
    $( "#date" ).datepicker({
      dateFormat: "yy-mm-dd"
    });
  } );
  </script>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Daftar Nama Tanda Tangan Surat Perjalan Dinas</h4>
                <p class="card-category">*Input Data Harus Benar</p>
            </div>
            <div class="card-body">
            <form  method="post">
                <div class="row">
                    <div class="col-md-12">
                    <div class="form-group">
                        <label class="">Nip</label>
                        <input type="number" class="form-control border-input" name="nip_spt" required>
                    </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="">Nama</label>
                            <input type="text" class="form-control border-input" name="nama_spt" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="">Jabatan</label>
                            <input  type="text" id="date" class="form-control border-input" name="jabatan_spt" required>
                        </div>
                    </div>
                </div>
                <div>
               
                <input type="reset" name="reset" class="btn btn-secondary btn-fill btn-wd">
                <input type="submit" name="kirim" value="Simpan" class="btn btn-info btn-fill btn-wd">   
            </div>
            </form>
        </div>
        </div>
    </div>
</div>

<?php
if(isset($_POST['kirim'])){
$nip_spt                = @$_POST['nip_spt'];
$nama_spt           = @$_POST['nama_spt'];
$jabatan_spt       = @$_POST['jabatan_spt'];

$cek = mysqli_num_rows(mysqli_query($koneksi,"SELECT * FROM tb_spt WHERE nip_spt='$nip_spt' or nama_spt='$nama_spt'"));
if ($cek > 0){ 
    echo "<script>window.alert('nama_spt atau email yang anda masukan sudah ada')
    window.location='?halaman=staf&aksi=tambah_staf'</script>";
}else{
    $sql = $koneksi->query("insert into tb_spt (nip_spt,nama_spt,jabatan_spt) values('$nip_spt
    ','$nama_spt','$jabatan_spt')");

    echo "<script>window.alert('Berhasil')
    window.location='?halaman=staf'</script>";

}
}
?>