<div class="row">
    <div class="col-md-12">
    <a href="./pdf/cetak_exel_dewan.php" target="blank"
                class="btn btn-info btn-sm glyphicon glyphicon-plus">
                Expor To exel
            </a>
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title ">Cetak Surat Dewan</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                      <tr>
                          <th>No</th>
                          <th>Nomor Surat</th>
                          <th>Nama Pelaksana</th>
                          <th>Maksud</th>
                          <th>Tanggal Berangkat</th>
                          <th>Tanggal Kembali</th>
                          <th>Action</th>
                      </tr>
                      </thead>
                        <tbody>
                        <?php
                                $no=1;
                                $sql=$koneksi->query("SELECT * FROM tb_sptd s 
                                INNER JOIN tb_pegawai d ON s.id = d.id
                                where sp_no");
                                while ($data= $sql->fetch_assoc()) {
                            ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $data['nomor_srtd']; ?></td>
                                <td><?php echo $data['nama']; ?></td>
                                <td><?php echo $data['maksud_sp']; ?></td>
                                <td><?php echo date('d F Y', strtotime($data['tgl_berangkat'])); ?></td>
                                <td><?php echo date('d F Y', strtotime($data['tgl_kembali'])); ?></td>
                                <td>
                                    <a href="./pdf/index1.php?sp_no=<?php echo $data['sp_no']?>" target="blank">  <span class="btn btn-info btn-sm">Cetak Surat</span></a>
                                    <a href="./pdf/cetak_sppd_dewan.php?sp_no=<?php echo $data['sp_no'] ?>" target="blank">  <span class="btn btn-info btn-sm">Cetak Sppd</span></a>
                                    <a href="?halaman=cetak1&aksi=pengikut&sp_no=<?php echo $data ['sp_no'];?>&tgl_berangkat=<?php echo $data['tgl_berangkat'];?>"> <span class="btn btn-info btn-sm">Tambah Pengikut</span></a>
                                    <a onclick="" href="?halaman=cetak1&aksi=hapus&sp_no=<?php echo $data ['sp_no']; ?>"  class="btn btn-danger btn-sm"></span>Hapus</a>
                                </td>
                            </tr>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
</div>
