<div class="row">
    <div class="col-md-12">
    <a href="./pdf/cetak_exel_staf.php" target="blank"
                class="btn btn-info btn-sm glyphicon glyphicon-plus">
                Expor To exel
            </a>
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title ">Cetak Surat Staff</h4>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor Surat</th>
                                        <th>Nama Pelaksana</th>
                                        <th>Maksud</th>
                                        <th>Tanggal Berangkat</th>
                                        <th>Tanggal Kembali</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                <?php
                                        $no=1;
                                        $sql=$koneksi->query("SELECT * FROM tb_surat s 
                                        INNER JOIN tb_staf d ON s.id_staf = d.id_staf
                                        where id_surat");
                                        while ($data= $sql->fetch_assoc()) {
                                    ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nomor_srt']; ?></td>
                                        <td><?php echo $data['nama_staf']; ?></td>
                                        <td><?php echo $data['maksud_sp']; ?></td>
                                        <!-- .date('d F Y', strtotime($result['tgl_berangkat'])) -->
                                        <td><?php echo date('d F Y', strtotime($data['tgl_berangkat'])); ?></td>
                                        <td><?php echo date('d F Y', strtotime($data['tgl_kembali'])); ?></td>
                                        <td>
                                            <a href="./pdf/index.php?id_surat=<?php echo $data['id_surat'] ?>" target="blank">  <span class="btn btn-info btn-sm">Cetak Surat</span></a>
                                            <a href="./pdf/cetak_sppd_staf.php?id_surat=<?php echo $data['id_surat'] ?>" target="blank">  <span class="btn btn-info btn-sm">Cetak Sppd</span></a>
                                            <!-- <a href="?halaman=cetak&aksi=pengikut&id_surat=<?php echo $data ['id_surat'];?>"> <span class="btn btn-info btn-sm">Tambah Pengikut</span></a> -->
                                            <a onclick="" href="?halaman=cetak&aksi=hapus&id_surat=<?php echo $data ['id_surat']; ?>"  class="btn btn-danger btn-sm"></span>Hapus</a>
                                        </td>
                                    </tr>
                                    <?php }
                                    ?>
                                </tbody>
                    
                    </table>
                  </div>
                </div>
              </div>
            </div>
</div>
