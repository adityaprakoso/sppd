<script>
  $( function() {
    $( "#date" ).datepicker({
      dateFormat: "yy-mm-dd"
    });
  } );
  </script>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Tambah Data Dewan</h4>
            </div>
            <div class="card-body">
            <form  method="post">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="">Nama Dewan</label>
                            <input type="text" class="form-control border-input" name="nama_dewan" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="">jabatan Dewan</label>
                            <input  type="text" id="date" class="form-control border-input" name="jabatan_dewan" required>
                        </div>
                    </div>
                </div>
                <div>
                <input type="reset" name="reset" class="btn btn-secondary btn-fill btn-wd">
                <input type="submit" name="kirim" value="Simpan" class="btn btn-info btn-fill btn-wd">   
            </div>
            </form>
        </div>
        </div>
    </div>
</div>

<?php
if(isset($_POST['kirim'])){
$nama_dewan           = @$_POST['nama_dewan'];
$jabatan_dewan       = @$_POST['jabatan_dewan'];

$cek = mysqli_num_rows(mysqli_query($koneksi,"SELECT * FROM tb_dewan WHERE  nama_dewan='$nama_dewan'"));
if ($cek > 0){ 
    echo "<script>window.alert('Nama Dewan yang anda masukan sudah ada')
    window.location='?halaman=dewan&aksi=tambah_dewan'</script>";
}else{
    $sql = $koneksi->query("insert into tb_dewan (nama_dewan,jabatan_dewan) values('$nama_dewan','$jabatan_dewan')");
    echo "<script>window.alert('Berhasil')
    window.location='?halaman=dewan'</script>";

}
}
?>