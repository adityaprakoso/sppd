<?php

$sp_no = @$_GET['sp_no'];
$sql  = $koneksi->query("select * from tb_sptd, tb_dewan where sp_no='$sp_no'");
$data_surat = $sql->fetch_assoc(); 
?>

<div class="row ">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title">Tambah Pengikut</h4>
                </div>
                <div class="card-body">
                  <form action="" method="post">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <input type="text" name="sp_no" class="form-control" value="<?= $data_surat['sp_no'] ?>" >
                          <input type="text" name="nama_dewan" class="form-control" value="<?= $data_surat['nama_dewan'] ?>" >
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group"> 
                          <label class="">nama Pengikut </label>
                          <select   class="form-control" name="nama_pengikut"  >
                          <?php
                              $sql = $koneksi->query("select * from tb_pegawai");
                              while ($data=$sql->fetch_assoc()) {
                                  echo "<option value='$data[nama]'>$data[nama]</option>";
                          }
                          ?>
                        </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group"> 
                          <label class="">Jabatan</label>
                          <select  class="form-control" name="jabatan_pengikut"  >
                          <?php
                          
                              $sql = $koneksi->query("select * from tb_pegawai");
                              while ($data=$sql->fetch_assoc()) {
                                  echo "<option value='$data[jabatan]'>$data[jabatan]</option>";
                          }
                          ?>
                        </select>
                        </div>
                      </div>
                    </div>
                    <div class="pull-center">
                        <input type="reset" class="btn btn-secondary" value="Reset">
                        <input type="submit" name="kirim" class="btn btn-info" value="Proses">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
<?php
if(isset($_POST['kirim'])){
    $sp_no = $_POST['sp_no'];
    $nama_pengikut = $_POST['nama_pengikut'];
    $jabatan_pengikut = $_POST['jabatan_pengikut'];
    $tgl_berangkat = $_GET['tgl_berangkat'];
    
//======== Cek Data Sama ========//
$cek = $koneksi->query("SELECT tgl_berangkat AS tgl_berangkat, tgl_kembali AS tgl_kembali FROM tb_sptd, tb_pengikut WHERE nama_pengikut='$nama_pengikut' ORDER BY tb_sptd.sp_no DESC ");
$cek = mysqli_fetch_assoc($cek);
// $cek = mysqli_num_rows($cek);
$tgl_berangkat_lama = $cek['tgl_berangkat'];
$tgl_kembali_lama = $cek['tgl_kembali'];
$cek_now_date = date("Y-m-d");

if($data_surat['nama_dewan']==$nama_pengikut){
  echo "<script>window.alert('Nama pengikut tidak bisa sama dengan nama pelaksana.'); </script>";
}
elseif($tgl_berangkat<=$tgl_kembali_lama){ 
  echo "<script>window.alert('Pengikut sedang dalam perjalanan, mohon cek tanggal kembali.'); </script>";
}
else{
    // mysqli_query($koneksi,"INSERT INTO tb_pengikut(sp_no,nama_pengikut,jabatan_pengikut) 
    //         VALUES ('$sp_no','$nama_pengikut','$jabatan_pengikut')");
    //         echo "<script>window.alert('Data Berhasil Simpan')
    //         window.location='?halaman=cetak1'</script>";
  echo "<script>window.alert('Data Berhasil Simpan.'); </script>";

}
}
    

?>
