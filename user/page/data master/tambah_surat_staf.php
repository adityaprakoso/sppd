<?php
    include "nomor_surat.php";
    $bulan    = date('n');
    $romawi    = getRomawi($bulan);
    $tahun     = date ('Y');

$carikode = mysqli_query($koneksi, "SELECT id_surat from tb_surat") or die (mysqli_error());
  // menjadikannya array
  $datakode = mysqli_fetch_array($carikode);
  $jumlah_data = mysqli_num_rows($carikode);
  // jika $datakode
  if ($datakode) {
   // membuat variabel baru untuk mengambil kode barang mulai dari 1
   $nilaikode = substr($jumlah_data[0], 1);
   // menjadikan $nilaikode ( int )
   $kode = (int) $nilaikode;
   // setiap $kode di tambah 1
   $kode = $jumlah_data + 1;
   // hasil untuk menambahkan kode 
   // angka 3 untuk menambahkan tiga angka setelah B dan angka 0 angka yang berada di tengah
   // atau angka sebelum $kode
   $kode_otomatis =str_pad($kode, 4, "0", STR_PAD_LEFT);
  } else {
   $kode_otomatis = "0001";
  }
//  echo "$interval->days hari "; // hasil : 217 hari
  ?>
<div class="row ">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title">SURAT STAF</h4>
                </div>
                <div class="card-body">
                  <form action="" method="post">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                        <label class="">Nomor Surat</label>
                          <input type="text" name="nomor_srt" class="form-control" 
                          value="094/    <?="/ST/SEKWAN/".$romawi."/".$tahun;?>" >
                        </div>
                      </div>

                      <div class="col-sm-6">
                        <div class="form-group">
                        <label class="">Dasar Surat</label>
                          <input type="text" name="dasar_srt" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="">Tanggal Berangkat</label>
                          <input type="date" class="form-control" name="tgl_berangkat" />
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group"> 
                          <label class="">Nama Pelaksana</label>
                          <select  class="form-control" name="id_staf">
                            <option value="">- Pilih Pelaksana -</option>
                            <?php
                                $query = mysqli_query($koneksi, "SELECT * FROM tb_staf");
                                while ($row = mysqli_fetch_array($query)) { ?>
                                <option value="<?php echo $row['id_staf']; ?>">
                                    <?php echo $row['nama_staf']. "/" .$row['nip_staf']; ?>
                                </option>
                            <?php } ?>
                        </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="">Tanggal Kembali</label>
                          <input type="date" class="form-control" name="tgl_kembali">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="">Jumlah hari</label>
                          <input type="number" name="jml_hari" value="<?php echo $interval;?>" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Kota Asal</label>
                          <input type="text" name="kota_asal"class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Kota Tujuan</label>
                          <input type="text" name="kota_tujuan" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                        <label class="">Pejabat Pembuat Komitmen</label>
                        <select id="pengikut" class="form-control" name="id_spt">
                            <option value="">- Pilih Pembuat Komitmen -</option>
                            <?php
                                $query = mysqli_query($koneksi, "SELECT * FROM tb_spt");
                                while ($row = mysqli_fetch_array($query)) { ?>
                                <option id="id_spt" value="<?php echo $row['id_spt']; ?>">
                                    <?php echo $row['nama_spt']; ?>
                                </option>
                            <?php } ?>
                        </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <select id="kendaraan" name="kendaraan" class="form-control">
                                  <option value="">Pilih Jenis Kendaraan</option>
                                  <option value="mobil">Mobil</option>
                                  <option value="pesawat">Pesawat</option>
                                  <option value="mobil-pesawat">Mobil - Pesawat</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="">Maksud Perjalanan Dinas</label>
                          <input type="text" name="maksud_sp" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                        <label class="">Tanggal Surat</label>
                          <input type="date" class="form-control" name="tgl_surat">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                        <select id="tingkat" name="tingkat" class="form-control">
                                  <option value="">Pilih Jenis tingkat</option>
                                  <option value="Tingkat A">Tingkat B</option>
                                  <option value="Tingkat B">Tingkat B</option>
                                  <option value="Tingkat C">Tingkat C</option>
                                  <option value="Tingkat D">Tingkat D</option>
                                  <option value="Tingkat E">Tingkat E</option>
                                  <option value="Tingkat F">Tingkat F</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="pull-center">
                        <input type="reset" class="btn btn-secondary" value="Reset">
                        <input type="submit" name="kirim" class="btn btn-info" value="Proses">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <?php
if(isset($_POST['kirim'])){
$nomor_srt      = @$_POST['nomor_srt'];
$dasar_srt      = @$_POST['dasar_srt'];
$tgl_berangkat = @$_POST['tgl_berangkat'];
$id_staf      = @$_POST['id_staf'];
$tgl_kembali = @$_POST['tgl_kembali'];
$kota_asal      =  @$_POST['kota_asal'];
$jml_hari       =  @$_POST['jml_hari'];
$kota_tujuan    = @$_POST['kota_tujuan'];
$id_spt = @$_POST['id_spt'];
$kendaraan      = @$_POST['kendaraan'];
$maksud_sp      = @$_POST['maksud_sp'];
$tgl_surat = @$_POST['tgl_surat'];
$tingkat = @$_POST['tingkat'];


 //======== Cek Data Sama ========//
$cek = $koneksi->query("SELECT tgl_berangkat AS tgl_berangkat, tgl_kembali AS tgl_kembali FROM tb_surat WHERE id_staf='$id_staf' ORDER BY nomor_srt DESC ");
$cek = mysqli_fetch_assoc($cek);
// $cek = mysqli_num_rows($cek);
$tgl_berangkat_lama = $cek['tgl_berangkat'];
$tgl_kembali_lama = $cek['tgl_kembali'];
$cek_now_date = date("Y-m-d");

  
  // if($tgl_kembali<$tgl_berangkat){
  //   echo "<script>window.alert('Tanggal lewat.');
  //   window.location='?halaman=sppd'</script>";
  // }
  if($tgl_kembali<$tgl_berangkat){
    echo "<script>window.alert('Pastikan tanggal berangkat dan tanggal kembali sudah benar.');
    window.location='?halaman=sppd'</script>";
  } 
  elseif($tgl_berangkat<=$tgl_kembali_lama){ 
    echo "<script>window.alert('Pelaksana sedang dalam perjalanan, mohon cek tanggal kembali.') </script>";
  }
  else{
    $sql = $koneksi->query("insert into tb_surat (nomor_srt,dasar_srt,tgl_berangkat,id_staf,tgl_kembali,kota_asal,
      jml_hari,kota_tujuan,id_spt,kendaraan,maksud_sp,tgl_surat,tingkat) 
      values ('$nomor_srt','$dasar_srt','$tgl_berangkat','$id_staf','$tgl_kembali','$kota_asal','$jml_hari',
      '$kota_tujuan','$id_spt','$kendaraan','$maksud_sp','$tgl_surat','$tingkat')");
      echo "<script>window.alert('Berhasil')
      window.location='?halaman=sppd'</script>";
  }
}

?>