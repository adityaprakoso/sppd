<div class="row">
    <div class="col-md-12">
            <a href="?halaman=staf&aksi=tambah_staf" 
                class="btn btn-info btn-sm glyphicon glyphicon-plus">
                Tambah Data
            </a>
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title ">DATA TTD STAF</h4>
                  <p class="card-category">List Data STAF Yang sudah daftar</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nip</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                        $no=1;
                                        $sql=$koneksi->query("select * from tb_spt");
                                        while ($data= $sql->fetch_assoc()) {
                                    ?>
                                      <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nip_spt']; ?></td>
                                        <td><?php echo $data['nama_spt']; ?></td>
                                        <td><?php echo $data['jabatan_spt']; ?></td>
                                        
                                        <!-- <td>
                                          <a href="?halaman=staf&aksi=ubah&nip_spt=<?php echo $data ['nip_spt']; ?>"> <span class="btn btn-secondary btn-sm">Edit</span></a>
                                          <a onclick="" href="?halaman=staf&aksi=hapus&nip_spt=<?php echo $data ['nip_spt']; ?>"  class="btn btn-danger btn-sm"></span>Hapus</a>
                                        </td> -->
                                         
                                      </tr>
                                    <?php }
                                    ?>
                                </tbody>
                    
                    </table>
                  </div>
                </div>
              </div>
            </div>
</div>
