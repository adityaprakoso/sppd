<script>
  $( function() {
    $( "#date" ).datepicker({
      dateFormat: "yy-mm-dd"
    });
  } );
  </script>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Daftar Nama Tanda Tangan Surat Perjalan Dinas</h4>
                <p class="card-category">*Input Data Harus Benar</p>
            </div>
            <div class="card-body">
            <form  method="post">
                <div class="row">
                    <div class="col-md-12">
                    <div class="form-group">
                        <label class="">Nip</label>
                        <input type="number" class="form-control border-input" name="nip_staf" required>
                    </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="">Nama</label>
                            <input type="text" class="form-control border-input" name="nama_staf" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="">Jabatan</label>
                            <input  type="text" id="date" class="form-control border-input" name="jabatan_staf" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="">Pangkat Golonagn</label>
                            <input  type="text" id="date" class="form-control border-input" name="pangkat_gol_staf" required>
                        </div>
                    </div>
                </div>
                <div>
               
                <input type="reset" name="reset" class="btn btn-secondary btn-fill btn-wd">
                <input type="submit" name="kirim" value="Simpan" class="btn btn-info btn-fill btn-wd">   
            </div>
            </form>
        </div>
        </div>
    </div>
</div>

<?php
if(isset($_POST['kirim'])){
$nip_staf                = @$_POST['nip_staf'];
$nama_staf           = @$_POST['nama_staf'];
$jabatan_staf       = @$_POST['jabatan_staf'];
$pangkat_gol_staf       = @$_POST['pangkat_gol_staf'];

$cek = mysqli_num_rows(mysqli_query($koneksi,"SELECT * FROM tb_spt WHERE nip_staf='$nip_staf' or nama_staf='$nama_staf'"));
if ($cek > 0){ 
    echo "<script>window.alert('nama_staf atau email yang anda masukan sudah ada')
    window.location='?halaman=staf&aksi=tambah_staf'</script>";
}else{
    $sql = $koneksi->query("insert into tb_staf (nip_staf,nama_staf,jabatan_staf,pangkat_gol_staf) values('$nip_staf
    ','$nama_staf','$jabatan_staf','$pangkat_gol_staf')");

    echo "<script>window.alert('Berhasil')
    window.location='?halaman=data_staf'</script>";

}
}
?>