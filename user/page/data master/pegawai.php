<div class="row">
    <div class="col-md-12">
            <a href="?halaman=master&aksi=tambah_pegawai" 
                class="btn btn-info btn-sm glyphicon glyphicon-plus">
                Tambah Data
            </a>
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title ">Data Dewan</h4>
                  <p class="card-category">List Data Dewan Yang sudah daftar</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <!-- <th>Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                        $no=1;
                                        $sql=$koneksi->query("select * from tb_pegawai");
                                        while ($data= $sql->fetch_assoc()) {
                                    ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nama']; ?></td>
                                        <td><?php echo $data['jabatan']; ?></td>
                                       
                                        <!-- <td>
                                            <a href="?halaman=master&aksi=ubah&id=<?php echo $data ['id']; ?>"> <span class="btn btn-secondary btn-sm">Edit</span></a>
                                            <a onclick="" href="?halaman=master&aksi=hapus&id=<?php echo $data ['id']; ?>"  class="btn btn-danger btn-sm"></span>Hapus</a>
                                        </td> -->
                                    
                                    </tr>
                                    <?php } ?>
                                </tbody>
                    
                    </table>
                  </div>
                </div>
              </div>
            </div>
</div>
