<?php

$id_surat = @$_GET['id_surat'];
$sql  = $koneksi->query("select * from tb_surat where id_surat='$id_surat'");
$data = $sql->fetch_assoc(); 
?>

<div class="row ">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title">Tambah Pengikut</h4>
                </div>
                <div class="card-body">
                  <form action="" method="post">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <input type="text" name="id_surat" class="form-control" value="<?= $data['id_surat'] ?>" >
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group"> 
                          <label class="">NAMA PENGIKUT</label>
                          <select   class="form-control" name="nama_pengikut"  >
                          <?php
                              $sql = $koneksi->query("select * from tb_staf");
                              while ($data=$sql->fetch_assoc()) {
                                  echo "<option value='$data[nama_staf]'>$data[nama_staf]</option>";
                          }
                          ?>
                        </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group"> 
                          <label class="">JABATAN PENGIKUT</label>
                          <select  class="form-control" name="jabatan_pengikut"  >
                          <?php
                              $sql = $koneksi->query("select * from tb_staf");
                              while ($data=$sql->fetch_assoc()) {
                                  echo "<option value='$data[jabatan_staf]'>$data[jabatan_staf]</option>";
                          }
                          ?>
                        </select>
                        </div>
                      </div>
                    </div>
                    <div class="pull-center">
                        <input type="reset" class="btn btn-secondary" value="Reset">
                        <input type="submit" name="kirim" class="btn btn-info" value="Proses">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
<?php
if(isset($_POST['kirim'])){
    $id_surat = $_POST['id_surat'];
    $nama_pengikut = @$_POST['nama_pengikut'];
    $jabatan_pengikut = $_POST['jabatan_pengikut'];


    $cek = $koneksi->query("SELECT tgl_berangkat AS tgl_berangkat, tgl_kembali AS tgl_kembali FROM tb_surat WHERE id_surat='$id_surat' ORDER BY nomor_srt DESC ");
    $cek = mysqli_fetch_assoc($cek);
    // $cek = mysqli_num_rows($cek);
    $tgl_berangkat_lama = $cek['tgl_berangkat'];
    $tgl_kembali_lama = $cek['tgl_kembali'];
    $cek_now_date = date("Y-m-d");


    if($tgl_berangkat<$cek_now_date || $tgl_kembali<$tgl_berangkat){
      echo "<script>window.alert('Pastikan tanggal berangkat dan tanggal kembali tanggal sudah benar.');
      window.location='?halaman=sppd'</script>";
    } 
    elseif($tgl_berangkat<=$tgl_kembali_lama){ 
      echo "<script>window.alert('Nama Sedang dalam Perjalanan Dinas') </script>";
    }
    else{
    mysqli_query($koneksi,"INSERT INTO tb_pengikut_staf(id_surat,nama_pengikut,jabatan_pengikut) 
            VALUES ('$id_surat','$nama_pengikut','$jabatan_pengikut')");
            echo "<script>window.alert('Data Berhasil Simpan')
            window.location='?halaman=cetak'</script>";
    }
}
?>
