<div class="row">
    <div class="col-md-12">
            <a href="?halaman=dewan&aksi=tambah_dewan" 
                class="btn btn-info btn-sm glyphicon glyphicon-plus">
                Tambah Data
            </a>
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title ">Data TTD Dewan</h4>
                  <p class="card-category">List Data Pegawai Yang sudah daftar</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                <?php
                                        $no=1;
                                        $sql=$koneksi->query("select * from tb_dewan");
                                        while ($data= $sql->fetch_assoc()) {
                                    ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nama_dewan']; ?></td>
                                        <td><?php echo $data['jabatan_dewan']; ?></td>
                                        <!-- <td>
                                            <a href="?halaman=dewan&aksi=ubah&id_dewan=<?php echo $data ['id_dewan']; ?>"> <span class="btn btn-secondary btn-sm">Edit</span></a>
                                            <a onclick="return confirm('Anda Yakin akan mengahapus Data..??')" 
                                            href="?halaman=dewan&aksi=hapus&id_dewan=<?php echo $data ['id_dewan']; ?>"  class="btn btn-danger btn-sm"></span>Hapus</a>
                                        </td> -->
                                    </tr>
                                    <?php 
                                    }
                                  ?>
                                </tbody>
                    
                    </table>
                  </div>
                </div>
              </div>
            </div>
</div>
