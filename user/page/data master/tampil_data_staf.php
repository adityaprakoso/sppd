<div class="row">
    <div class="col-md-12">
            <a href="?halaman=data_staf&aksi=tambah_data_staf" 
                class="btn btn-info btn-sm glyphicon glyphicon-plus">
                Tambah Data
            </a>
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title ">DATA STAFF</h4>
                  <p class="card-category">List Data STAF Yang sudah daftar</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nip</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>Pangkat Golongan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                <?php
                                        $no=1;
                                        $sql=$koneksi->query("select * from tb_staf");
                                        while ($data= $sql->fetch_assoc()) {
                                    ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nip_staf']; ?></td>
                                        <td><?php echo $data['nama_staf']; ?></td>
                                        <td><?php echo $data['jabatan_staf']; ?></td>
                                        <td><?php echo $data['pangkat_gol_staf']; ?></td>
                                        <!-- <td>
                                        <a href="?halaman=data_staf&aksi=ubah&nip_staf=<?php echo $data ['nip_staf']; ?>"> <span class="btn btn-secondary btn-sm">Edit</span></a>
                                        <a onclick="" href="?halaman=data_staf&aksi=hapus&nip_staf=<?php echo $data ['nip_staf']; ?>"  class="btn btn-danger btn-sm"></span>Hapus</a>
                                        </td> -->
                                    </tr>
                                    <?php }
                                    ?>
                                </tbody>
                    
                    </table>
                  </div>
                </div>
              </div>
            </div>
</div>
